import sublime
import sublime_plugin
import requests
import json

#Constantes
GESTAR_SYNTAX_PATH = "Packages/User/VBScript_gestar.sublime-syntax"

auth_token = None

class GetDoorsVersionCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        version = call('doorsversion', 'GET', {}, {})
        sublime.message_dialog(version)

class ShowCodeLibsCommand(sublime_plugin.WindowCommand):
    def run(self):
        self.items = []
        self.fld_id = -1

        self.window.show_input_panel("Ingrese el fld_id:", "", self.show_code_libs, None, None)

    def open_code_lib(self, index):
        print("index"+ str(index))
        index = int(index)
        if index >= 0:
            sel_code_lib = self.items[int(index)]
            if self.window.active_view():
                self.window.active_view().run_command("open_code_lib", {"codelib_fld_id": self.fld_id, "codelib_name": sel_code_lib[0]})
                self.window.active_view().run_command("set_file_type", {"syntax": GESTAR_SYNTAX_PATH})

    def show_code_libs(self, fld_id):
        self.fld_id = fld_id
        code_libs = folderSearch(fld_id, fields="NAME, DESCRIPTION", formula="", maxDescrLength=1000000000)
        #print(code_libs)
        for code_lib in code_libs:
            self.items.append([code_lib['NAME'], code_lib['DESCRIPTION'] or code_lib['NAME']])
        #print(self.items)
        self.window.show_quick_panel(self.items, self.open_code_lib)

class openCodeLibCommand(sublime_plugin.TextCommand):
    def run(self, edit, codelib_fld_id, codelib_name):
        # TODO: ver esta constante de maxDescrLength probablemente haya que usar 'GET /documents/{documentId}'
        # en vez del metodo 'GET /folders/{fldId}/documents' 
        data = folderSearch(codelib_fld_id, fields="NAME, CODE", formula="NAME = '%s'" % codelib_name, maxDescrLength=1000000000)
        print(data)
        if data:
            code = data[0]['CODE']
            name = data[0]['NAME']
            window = self.view.window()
            new_view = window.new_file()
            new_view.set_name(name)
            # tomo todo el contenido
            #allcontent = sublime.Region(0, new_view.size())
            # reemplazo todo el contenido por el codigo de la biblioteca
            if code:
                #new_view.replace(edit, allcontent, code.replace('\r', ''))
                new_view.insert(edit, 0, code.replace('\r', ''))
            else:
                #new_view.replace(edit, allcontent, 'La bliblioteca NO tiene codigo!')
                new_view.insert(edit, 0, 'La biblioteca NO tiene codigo!')


def plugin_loaded():
    print("GetDoorsVersionCommand just got loaded")
    logon()

# def logon():
#     global auth_token

#     settings = sublime.load_settings("connections.sublime-settings")
#     connections = settings.get("connections")
#     #TODO: ver como modificar esto para poder especificar una conexion default y cargarla aca
#     loginInfo = connections[0]['loginInfo']
#     server = connections[0]['server']
#     url = server + '/restful/session/logon'
#     resp = requests.post(url=url, json=loginInfo)
#     #Preguntar a santi porque viene encodeado en utf8 con BOM!
#     #Esta linea le saca que BOM porque si no json.loads falla
#     data = json.loads(resp.content.decode('utf-8-sig'))
#     auth_token = data['InternalObject']

def call_debug(method, httpMethod, jsonParams, params):
    global auth_token

    settings = sublime.load_settings("connections.sublime-settings")
    connections = settings.get("connections")
    #TODO: ver como modificar esto para poder especificar una conexion default y cargarla aca
    server = connections[0]['server']
    url = server + '/restful/' + method
    #headers = {'AuthToken': auth_token}
    headers = {'AuthToken': "473F0E01863D5CA6095CEAB742A7FC515C7E16519B6F0A11A1E55B385DD9FA4F"}
    #resp = requests.request(httpMethod, url=url, json=jsonParams, params=params, headers=headers)
    req = requests.Request(httpMethod, url=url, params=params, headers=headers)
    prepared = req.prepare()
    pretty_print_POST(prepared)
    #data = json.loads(resp.content.decode('utf-8-sig'))
    #print(resp.content.decode('utf-8-sig'))
    #print(params)
    #print(url)
    #pretty_print_POST()
    #return data['InternalObject']
    s = requests.Session()
    resp = s.send(prepared)
    return resp.text

def call(method, httpMethod, jsonParams, params):
    global auth_token

    settings = sublime.load_settings("connections.sublime-settings")
    connections = settings.get("connections")
    #TODO: ver como modificar esto para poder especificar una conexion default y cargarla aca
    server = connections[0]['server']
    url = server + '/restful/' + method
    headers = {'AuthToken': auth_token}
    resp = requests.request(httpMethod, url=url, json=jsonParams, params=params, headers=headers)
    data = json.loads(resp.content.decode('utf-8-sig'))
    return data['InternalObject']

def logon():
    global auth_token

    settings = sublime.load_settings("connections.sublime-settings")
    connections = settings.get("connections")
    #TODO: ver como modificar esto para poder especificar una conexion default y cargarla aca
    loginInfo = connections[0]['loginInfo']
    data = call('session/logon', 'POST', loginInfo, {})
    auth_token = data

def isLoggued():
    return call('session/islogged', 'POST', {}, {})

def getDocumentById(doc_id):
    return call('documents/%s' % doc_id, 'GET', {}, {})

def folderSearch(fld_id, fields=None, formula=None, order="", maxDocs=1000, recursive=False, maxDescrLength=100):
    params = {
        "fields": fields,
        "formula": formula,
        "order": order,
        "maxDocs": maxDocs,
        "recursive": recursive,
        "maxDescrLength": maxDescrLength
    }
    url = "folders/%s/documents" % fld_id
    return call(url, 'GET', {}, params)

def pretty_print_POST(req):
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in 
    this function because it is programmed to be pretty 
    printed and may differ from the actual request.
    """
    print('{}\n{}\n{}\n\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))
