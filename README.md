**Esto es una POC no es la idea que el plugin final tenga este diseño e interfaz**

Básicamente este plugin provee dos comandos:

1. Gestar Doors Version: muestra en un popup la versión.(super simple casi un hello word)
2. Mostrar bibliotecas: pide al usuario que ingrese un fld_id (de una carpeta cuyos documentos sean codelibs), acontinuacion
	lista todas las codelibs de ese folder por nombre y descripción, ordenadas alfabeticamente por NAME y con el autocomplete
	de sublime. Finalmente al seleccionar una codelib se abren los contenidos de esta en un nuevo tab.(TODO: el save)

---

## Instalacion

Es un viaje...

1. Instalar sublime text 3
2. Instalar package control
	1. abrir sublime y ctrl+shif+p install package control
3. Instalar plugin manualmente
	1. Copiar la carpeta "gestarRemote" en "C:\Users\Admin\AppData\Roaming\Sublime Text 3\Packages", para ubicar esta
		carpeta más facilmente, en st (sublime text), ir a preferences->Browse Packages se abrirá automaticamente la
		ubicacion mencionada.
4. Instalar la sintaxis:
	1. Ir a la carpeta: "C:\Users\[user]\AppData\Roaming\Sublime Text 3\Packages\User" copiar el contenido de la carpeta
		"sintaxis" (para ver si se instaló correctamente tipear ctrl+shift+p y escribir "gestar" debería aparecer el
		comando para cambiar la sintaxis del archivo actual)
5. Ejecutar el comando "Package Control: Satisfy Dependencies" (el plugin utiliza el modulo request, este comando instala
	el modulo)
6. Reiniciar sublime, podría salir un mensaje de advertencia, ignorarlo y reiniciar nuevamente.
7. Fin

---

## Demo

https://youtu.be/opnJ5o3bzfM